### Adding releases names to downloads cards data file

To add releases names to downloads cards data file, edit the downloads cards
data file, and add a release key and value to `releases` dictionary. For
example, if you want to add release names `9` and `8` to your downloads cards
data file, the `releases` property should have two release dictionaries. One
for `9` and one for `8`.

```
---
- releases:
   9: {}
   8: {}
```

In this example, both `9` and `8` releases are empty. They do not include
any release-dependent information.

When the page loads, the first release key you entered in the `releases`
dictionary (i.e., `9`, in the example above) is the active release in the
release selector. Subsequent release keys are presented in the same order they
were entered in the `releases` dictionary from top to bottom.
