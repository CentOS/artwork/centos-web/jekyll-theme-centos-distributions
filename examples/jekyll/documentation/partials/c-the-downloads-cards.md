### The downloads cards

The downloads cards exist to present download information in your site pages.

The downloads cards is the result of combining the downloads cards presentation
template and the downloads cards data file. This combination happens when you
add downloads cards to pages.

Related Concepts:

- [The downloads cards presentation template](#the-downloads-cards-presentation-template)
- [The downloads cards data file](#the-downloads-cards-data-file)

Related Tasks:

- [Adding downloads cards to pages](#adding-downloads-cards-to-pages)
