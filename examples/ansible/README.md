# The `examples/ansible` directory

This directory structure provides ansible inventory and playbook required to
build websites consistent with
[jekyll-theme-centos](https://gitlab.com/CentOS/artwork/centos-web/jekyll-theme-centos/).
Such files, however, are not present in this directory because they are copied
from
[jekyll-theme-centos-base:examples/ansible](https://gitlab.com/CentOS/artwork/centos-web/jekyll-theme-centos-base/-/tree/main/examples/ansible),
when the pipeline runs.

To see the files copied when the pipeline runs, go to [jekyll-theme-centos-base:examples/ansible](https://gitlab.com/CentOS/artwork/centos-web/jekyll-theme-centos-base/-/tree/main/examples/ansible).
