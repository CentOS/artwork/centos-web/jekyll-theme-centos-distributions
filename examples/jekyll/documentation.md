---
title: Documentation
title_lead: |
  This page describes jekyll-theme-centos-download component and how you can
  use it in your sites.

with_toc: true
with_highlight: stackoverflow-light
---

## Concepts

{% include_relative documentation/partials/c-the-downloads-cards.md %}
{% include_relative documentation/partials/c-the-downloads-cards-presentation-template.md %}
{% include_relative documentation/partials/c-the-downloads-cards-data-file.md %}

## Tasks

{% include_relative documentation/partials/t-adding-downloads-cards-to-pages.md %}
{% include_relative documentation/partials/t-adding-releases-to-downloads-cards.md %}
{% include_relative documentation/partials/t-adding-releases-screenshots-to-downloads-cards.md %}
{% include_relative documentation/partials/t-adding-releases-mirrors-to-downloads-cards.md %}
{% include_relative documentation/partials/t-adding-releases-docs-to-downloads-cards.md %}
{% include_relative documentation/partials/t-adding-releases-eol-to-downloads-cards.md %}
{% include_relative documentation/partials/t-adding-releases-cmds-to-downloads-cards.md %}

## References

This documentation tries to follow [The DITA Style Guide Best Practices for Authors](https://www.oxygenxml.com/dita/styleguide/).
