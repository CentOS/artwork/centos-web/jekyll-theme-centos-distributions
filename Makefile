all:

.PHONY: site-sources
site-sources:
	install -D -t examples/jekyll/_includes/download  src/HTML/jekyll/_includes/*
	install -D -t examples/jekyll/_layouts/download   src/HTML/jekyll/_layouts/*
	install -D -t examples/jekyll/_data/download      src/YAML/*
	install -D -t examples/jekyll/assets/img/download src/IMG/*

site-prettier: site
	podman run --rm \
		--name $@ \
		-v $$PWD/public:/public:z \
		registry.gitlab.com/centos/artwork/centos-web/pipelines/toolbox/npm-prettier:latest \
		npx prettier --ignore-path .prettierignore -w "/public/**/*.html"

site-l10n:
	podman run --rm \
		--name $@ \
		-v $$PWD/examples/jekyll:/site:z \
		-v $$PWD/public:/public:z \
		--env JEKYLL_SOURCE_DIR=/site \
		--env JEKYLL_PUBLIC_DIR=/public \
		registry.gitlab.com/centos/artwork/centos-web/templates/jekyll-site-l10n:latest \
		/usr/local/bin/page-locale.sh

.PHONY: site-server
site-server: clean site-sources
	install -d public
	podman run --rm -d \
		--name $@ \
		-v $$PWD/public:/public \
		-v $$PWD/examples/jekyll:/site \
		-p 0.0.0.0:4000:4000 \
		registry.gitlab.com/centos/artwork/centos-web/jekyll-theme-centos:latest \
		bundle exec jekyll serve -H 0.0.0.0 -p 4000 --config /site/_config.yml -s /site -d /public

.PHONY: site
site: site-sources
	install -d public
	podman run --rm \
		--name $@ \
		-v $$PWD/public:/public \
		-v $$PWD/examples/jekyll:/site \
		registry.gitlab.com/centos/artwork/centos-web/jekyll-theme-centos:latest \
		bundle exec jekyll build --baseurl "/artwork/centos-web/jekyll-theme-centos-download" --config /site/_config.yml -s /site -d /public

.PHONY: site-ansible
site-ansible:
	install -d public-ansible
	podman run --rm \
		--name $@ \
		-v $$PWD/src:/src \
		-v $$PWD/public-ansible:/public \
		-v $$PWD/examples/ansible:/site \
		registry.gitlab.com/centos/artwork/centos-web/jekyll-theme-centos-base:latest \
		ansible-playbook -i /site/inventory \
		-e 'public_dir="/public"' \
		-e 'src_dir="/src"' \
		-e 'component_name="download"' \
		-e 'component_description="HTML templates, YAML files and images related to jekyll-theme-centos-download."' \
		/site/playbook.yml

site-ansible-prettier: site-ansible
	podman run --rm \
		--name $@ \
		-v $$PWD/public-ansible:/public:z \
		registry.gitlab.com/centos/artwork/centos-web/pipelines/toolbox/npm-prettier:latest \
		npx prettier --ignore-path .prettierignore -w "/public/index.html"

site-html-differences: site-prettier site-ansible-prettier
	diff -s public/index.html public-ansible/index.html

ci_source_branch   := update-pipeline-components-version
ci_target_branch   := staging
ci_catalog_version := $(shell curl --silent "https://gitlab.com/api/v4/projects/47140526/releases?sort=desc" | jq -r '.[0].tag_name')
ci_commit_message  := ci: 🎡 update pipeline components to $(ci_catalog_version)
.PHONY: ci-update-version
ci-update-version:
	git checkout $(ci_target_branch)
	git pull
	git checkout -b $(ci_source_branch)
	sed -i -r -e "s/@(.+)$$/@$(ci_catalog_version)/" .gitlab-ci.yml
	git add .gitlab-ci.yml
	git commit -m "$(ci_commit_message)" .gitlab-ci.yml
	glab mr create \
		--title "$(ci_commit_message)" \
		--target-branch "$(ci_target_branch)" \
		--description "" \
		--no-editor \
		--push \
		--squash-before-merge \
		--remove-source-branch
	#glab ci status --live
	#git checkout $(ci_target_branch)
	#git pull
	#git branch -d "$(ci_source_branch)"
	#glab mr merge $(ci_source_branch) --auto-merge --squash --squash-message "$(ci_commit_message)"
	#glab ci status --live

.PHONY: clean
clean:
	$(RM) -r examples/jekyll/_includes
	$(RM) -r examples/jekyll/_layouts
	$(RM) -r examples/jekyll/_data
	$(RM) -r examples/jekyll/_sass
	$(RM) -r examples/jekyll/assets
	$(RM) -r examples/ansible/files
	$(RM) -r public
	$(RM) -r public-ansible
