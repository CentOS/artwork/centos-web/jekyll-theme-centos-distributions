# jekyll-theme-centos-download

This project provides the source files and automation scripts used to build the
CentOS download component in CentOS websites.

Live example: https://centos.gitlab.io/artwork/centos-web/jekyll-theme-centos-download

## License

MIT
