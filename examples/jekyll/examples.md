---
title: Examples

with_highlight: stackoverflow-light
with_toc: true
---

## Example 1: Presenting all card names without restriction

```
{%- raw -%}
{% include download/cards.html id="DownloadsCard-1"
with_title=true
with_description=true
with_releases=true
with_screenshots=true
with_mirrors=true
with_documentation=true
with_eol=true
with_commands=true
%}
{% endraw %}
```

{% include download/cards.html id="DownloadsCard-1"
with_title=true
with_description=true
with_releases=true
with_screenshots=true
with_mirrors=true
with_documentation=true
with_eol=true
with_commands=true
%}

## Example 2: Presenting all card names with restrictions

```
{%- raw -%}
{% include download/cards.html id="DownloadsCard-2"
with_title=true
with_description=false
with_releases=true
with_screenshots=true
with_mirrors=false
with_documentation=false
with_eol=true
with_commands=false
%}
{% endraw %}
```

{% include download/cards.html id="DownloadsCard-2"
with_title=true
with_description=false
with_releases=true
with_screenshots=true
with_mirrors=false
with_documentation=false
with_eol=true
with_commands=false
%}

## Example 3: Presenting only one card name with restrictions

```
{%- raw -%}
{% assign cards = site.data.download.cards | where: "title", "Fictitious Stream Distribution 1"%}
{% include download/cards.html id="DownloadsCard-3" data=cards
with_title=true
with_description=false
with_releases=true
with_screenshots=false
with_mirrors=false
with_documentation=false
with_eol=true
with_commands=true
%}
{% endraw %}
```
{% assign cards = site.data.download.cards | where: "title", "Fictitious Stream Distribution 1"%}
{% include download/cards.html id="DownloadsCard-3" data=cards
with_title=true
with_description=false
with_releases=true
with_screenshots=false
with_mirrors=false
with_documentation=false
with_eol=true
with_commands=true
%}
