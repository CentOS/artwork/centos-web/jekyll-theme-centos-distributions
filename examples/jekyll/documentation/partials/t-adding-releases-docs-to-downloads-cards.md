### Adding releases documentation to downloads cards data file

To add releases documentation to downloads cards data file, edit the downloads
data file and add the `documentation` property into the release key
accordingly. The value of `mirrors` property is a list of dictionaries
describing the relation between release name, and documentation links for the
named distribution.

```
---
- title: "CentOS Stream"
  releases:
    9:
      documentation:
        - title: Release Notes
          link: "https://blog.centos.org/2021/12/introducing-centos-stream-9/"
        - title: Release Email
          link: "https://lists.centos.org/pipermail/centos-announce/2021-December/060971.html"
        - title: Website
          link: "https://docs.centos.org/en-US/docs/"
```

{:.table .table-bordered}
| Property | Description |
| -------- | ----------- |
| `title` | The documentation name. |
| `link` | The documentation link. |
