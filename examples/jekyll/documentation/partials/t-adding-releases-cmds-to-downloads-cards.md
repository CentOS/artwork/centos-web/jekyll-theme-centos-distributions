### Adding releases commands to downloads cards data file

To add releases commands to downloads cards data file, edit the downloads data
file and add the `commands` property into the release key accordingly. The
value of `commands` property is a list of dictionaries describing the relation
between release name, and auxiliary commands you can run in the named
distribution to do specific stuff.

```
---
- title: "CentOS Linux"
  releases:
    8:
      commands:
        - title: "Converting from CentOS Linux 8 to CentOS Stream 8"
          lines: |-
            dnf --disablerepo '*' --enablerepo extras swap centos-linux-repos centos-stream-repos
            dnf distro-sync
```

{:.table .table-bordered}
| Property | Description |
| -------- | ----------- |
| `title` | A string describing the end of life date. For example, `31 May 2024`. |
| `description` | The end of life description. Useful when you need to describe what happens during the end of life date, or provide a link to external resources explaining it. |
