### Adding releases end of life to downloads cards data file

To add releases end of life to downloads cards data file, edit the downloads
data file and add the `eol` property into the release key accordingly. The
value of `eol` property is a list of dictionaries describing the relation
between release name, and end of life date for the named distribution.

```
---
- title: "CentOS Stream"
  releases:
    9:
      eol:
        - date: ""
          description: |
            End of RHEL9 <a href="https://access.redhat.com/support/policy/updates/errata#Full_Support_Phase">full support</a> phase.
```

{:.table .table-bordered}
| Property | Description |
| -------- | ----------- |
| `date` | The end of life date. For example, `31 May 2024`. |
| `description` | The end of life description. Useful if you need to explain what happens during the end of life date, or provide a link to external resources explaining it. |
