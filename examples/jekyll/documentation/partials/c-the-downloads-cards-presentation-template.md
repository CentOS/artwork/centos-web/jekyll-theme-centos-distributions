### The downloads cards presentation template

The downloads cards presentation template organizes download information
vertically, using title, description, and release selector fields. The title
and description provide common information to releases exposed through the
release selector. The release selector controls the visibility of
release-dependent information like screenshots, mirrors, documentation, end of
life and commands.

The downloads cards presentation template allows you to customize the
visibility of all its elements using include arguments. [Fig. The downloads
cards presentation template](#fig-the-downloads-cards-presentation-template)
illustrates the vertical organization of components inside the downloads cards
presentation template and their respective include arguments.

{% include base/image.html
file="download/fig-the-downloads-cards-presentation-template.png"
alt="fig-the-downloads-cards-presentation-template"
caption="Fig. The downloads cards presentation template"
%}

The downloads cards presentation template does not provide a way to change the
vertical presentation order of its elements. However, you can include several
downloads cards presentation templates in the same page and control the
visibility of their components using include arguments to reach the vertical
organization you need.

The downloads cards presentation template reads download information from the
downloads cards data file.
