### The downloads cards data file

The downloads cards data file provides download information for the downloads
cards presentation template.

The downloads cards data file default location is
`src/_data/download/cards.yml`. When this file does not exist in your site, it
is read from the theme. When you create this file in your site, then it is used
instead.

The downloads cards data file is a list of dictionaries written in YAML format.
The simplest dictionary structure you can provide on each list item contains
the `title`, `description`, and `releases` properties.

```
---
- title: "CentOS Stream"
  description: |
    Continuously delivered distro that tracks just ahead of Red Hat Enterprise
    Linux (RHEL) development, positioned as a midstream between Fedora Linux
    and RHEL. For anyone interested in participating and collaborating in the
    RHEL ecosystem, CentOS Stream is your reliable platform for innovation.
  releases: []
```

The following table describes the meaning of these three configuration
properties:

{:.table .table-bordered}
| Property | Type | Description |
| -------- | ---- | ----------- |
| `title` | `""` | The name of the GNU/Linux distribution you are providing releases for. |
| `description` | `""` | A brief description about the GNU/Linux distribution you are providing releases for. |
| `releases` | `[]` | A list of dictionaries describing the GNU/Linux distribution release names and their respective dependencies. |

{:.alert .alert-info}
**NOTE:** The `releases` property configuration is not described in this
section. Instead, the documentation uses task-oriented sections below to
describe each possible configuration you can perform.

The `release` property accepts the following configurations:

- [Adding releases names to downloads cards data file](#adding-releases-names-to-downloads-cards-data-file)
- [Adding releases screenshots to downloads cards data file](#adding-releases-screenshots-to-downloads-cards-data-file)
- [Adding releases mirrors to downloads cards data file](#adding-releases-mirrors-to-downloads-cards-data-file)
- [Adding releases documentation to downloads cards data file](#adding-releases-documentation-to-downloads-cards-data-file)
- [Adding releases end of life to downloads cards data file](#adding-releases-end-of-life-to-downloads-cards-data-file)
- [Adding releases commands to downloads cards data file](#adding-releases-commands-to-downloads-cards-data-file)
