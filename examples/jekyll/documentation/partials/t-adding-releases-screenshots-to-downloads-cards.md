### Adding releases screenshots to downloads cards data file

To add releases screenshots to downloads cards data file, do the following:

1. Create the `src/assets/img/download/screenshots/` directory.

1. Copy your screenshot files o `src/assets/img/download/screenshots/`
   directory. The screenshot files titles follow the
   `<title>-<release>-screenshot-<N>.png` naming convention.

   {:.table .table-bordered}
   | Field | Description |
   | ----- | ----------- |
   | `<title>` | The GNU/Linux distribution name. This is an alphabetic string, in lowercase with space characters replaced by dash (`-`) character. |
   | `<release>` | The GNU/Linux distribution release. For consistency, it should be the same value in the `releases` key. |
   | `<N>` | Incremental integer to make screenshot files unique per `<title>` and `<release>`. |

1. Edit the downloads data file and add the `screenshots` property into the
   release key accordingly. For example, if the screenshots you copied are for
   CentOS Stream 9, the configuration would be like the following:

   ```
   ---
   - title: CentOS Stream
     releases:
       9:
         screenshots:
           - src: "centos-stream-9-screenshot-1.png"
             alt: "centos-stream-9-screenshot-1.png"
   ```

   {:.table .table-bordered}
   | Property | Description |
   | -------- | ----------- |
   | `src` | The screenshot image file title, relative to `src/assets/img/download/screenshots/` directory. |
   | `alt` | The screenshot image alternative text. |
