### Adding downloads cards to pages

To add the downloads cards to pages, do the following:

1. Include `download/card.html`, the downloads cards presentation template.

   ```
   {%- raw -%}
   {% include download/cards.html %}
   {% endraw %}
   ```

1. Provide the include arguments to control the presentation.

   ```
   {%- raw -%}
   {% include download/cards.html id="DownloadsCard-1"
   with_title=false
   with_description=false
   with_release=true
   with_mirrors=true
   %}
   {% endraw %}
   ```

   {:.table .table-bordered}
   | Argument | Default | Description |
   | -------- | ---- | ----------- |
   | `id` | `"DownloadsCard-1"` | The download card HTML identifier. This value must be unique in the entire page. Relevant when you are includeing more than one downloads card in the same page. |
   | `data` | `site.data.download.cards` | The data variable holding the content of downloads cards. |
   | `title` | `""` | The exact downloads card title you want to present. When you pass this argument the downloads cards presentation template limits the amount of information it output for that specific card title you provided. |
   | `with_title` | `false` | Present the download cards title. Possible values are `false` or `true`. |
   | `with_description` | `false`| Present the download cards description. Possible values are `false` or `true`.|
   | `with_release` | `false` | Present the download card release-specific content. Possible values are `false` or `true`. |
   | `with_screenshots` | `false` | Present release-specific screenshots. Possible values are `false` or `true`. Requires `with_release: true`. |
   | `with_mirrors` | `false` | Present release-specific mirrors. Possible values are `false` or `true`. Requires `with_release: true`. |
   | `with_documentation` | `false` | Present release-specific documentation. Possible values are `false` or `true`. Requires `with_release: true`. |
   | `with_eol` | `false` | Present release-specific end of life. Possible values are `false` or `true`. Requires `with_release: true`. |
   | `with_commands` | `false` | Present release-specific commands. Possible values are `false` or `true`. Requires `with_release: true`. |
