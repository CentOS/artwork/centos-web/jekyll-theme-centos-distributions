### Adding releases mirrors to downloads cards data file

To add releases mirrors to downloads cards data file, edit the downloads data
file and add the `mirrors` property into the release key accordingly. The value
of `mirrors` property is a list of dictionaries describing the relation between
architecture, release name, and download links for the named distribution in
different formats.

```
---
- title: "CentOS Stream"
  releases:
    9:
      mirrors:
        - architecture: "x86_64"
          iso: "https://mirrors.centos.org/mirrorlist?path=/9-stream/BaseOS/x86_64/iso/CentOS-Stream-9-latest-x86_64-dvd1.iso&redirect=1&protocol=https"
          rpm: "http://mirror.stream.centos.org/9-stream/"
          cloud: "https://cloud.centos.org/centos/9-stream/x86_64/images"
          containers: "https://quay.io/centos/centos:stream9"
          vagrant: ""
```

{:.table .table-bordered}
| Property | Description |
| -------- | ----------- |
| `architecture` | The architecture name the mirrors provide content for. For example, `x86_64`, `ARM64 (aarch64)`, `IBM Power (ppc64le)`, `IBM Z (s390x)`. |
| `iso` | The download mirror URL where you can find ISO files for the `architecture` defined. |
| `rpm` | The download mirror URL where you can find RPM packages for the `architecture` defined. |
| `cloud` | The URL where you can find cloud images for the `architecture` defined. |
| `containers` | The URL where you can find container images for the `architecture` defined. |
| `vagrant` | The URL where you can find Vagrant boxes for the `architecture` defined. |
